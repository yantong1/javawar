package webController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MvcController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/mvc")
    @ResponseBody
    public String mvc() {
        return "Hello Spring MVC";
    }
}