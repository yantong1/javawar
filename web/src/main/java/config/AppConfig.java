package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan({"webController","childController"})
@EnableWebMvc
public class AppConfig implements WebMvcConfigurer {
    /**
     * WebMvcConfigurer中的十多个方法基本上涵盖了spring mvc需要配置的内容，
     * 如：跨域配置、视图解析器、参数解析器、拦截器、消息转换器等等，可以按需求重写
     */

    /**
     * 配置视图解析器
     * @param registry
     */
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp("/WEB-INF/", ".html");
    }

    /**
     * 配置资源映射，否则访问不了html
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/WEB-INF/**").addResourceLocations("/WEB-INF/");
    }
}
